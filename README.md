

1. Initailise the local repo:

$ cd $HOME
$ mkdir config-repo
$ cd config-repo
$ git init .
$ echo info.foo: bar > application.properties
$ git add -A .
$ git commit -m "Add application.properties" 

2. Start the spring cloud config server:

mvn spring-boot:run

3. Start the spring cloud config client

mvn spring-boot:run

